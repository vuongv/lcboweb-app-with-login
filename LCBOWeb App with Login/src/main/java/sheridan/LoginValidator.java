package sheridan;

public class LoginValidator {

	public static boolean isValidLoginNameLength( String loginName ) {
		return loginName.length( ) >= 6 ;
	}
	public static boolean isValidLoginNameAlphaNumeric(String loginName) {
		boolean returnCode = false;
		boolean firstCharacterNotNumber = true;
		int alphaNumericCounter = 0;
		if(loginName.charAt(0) >= '0' && loginName.charAt(0) <= '9') {
			firstCharacterNotNumber = false;
		}
		if (firstCharacterNotNumber){
			for (int i = 0 ; i < loginName.length() ; i++) {
				if((loginName.charAt(i) >= 'a' && loginName.charAt(i) <= 'z') || (loginName.charAt(i) >= 'A' && loginName.charAt(i) <= 'Z') || ((loginName.charAt(i) >= '0' && loginName.charAt(i) <= '9'))){
					alphaNumericCounter ++;
				}
			}
			if(alphaNumericCounter == loginName.length()) {
				returnCode = true;
			}
			else {
				returnCode = false;
			}
		}
		
		return returnCode;
	}
}
