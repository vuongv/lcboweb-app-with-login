package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginNameLengthRegular( ) {
		boolean result = LoginValidator.isValidLoginNameLength("henrys");
		assertTrue("Invalid login" , result );
	}
	@Test
	public void testIsValidLoginNameLengthExeception() {
		boolean result = LoginValidator.isValidLoginNameLength("henry");
		assertFalse("invalid Login", result);
	}
	@Test
	public void testIsValidLoginNameLengthBoundaryIn() {
		boolean result = LoginValidator.isValidLoginNameLength("henry9");
		assertTrue("Invalid Login", result);
	}
	@Test
	public void testIsValidLoginNameLengthBoundaryOut() {
		boolean result = LoginValidator.isValidLoginNameLength("aaaaa");
		assertFalse("Invalid Login", result);
	}
	@Test
	public void testIsValidLoginNameAlphaNumericRegular() {
		boolean result = LoginValidator.isValidLoginNameAlphaNumeric("henrys");
		assertTrue("Invalid Login", result);
	}
	@Test
	public void testIsValidLoginNameAlphaNumericException() {
		boolean result = LoginValidator.isValidLoginNameAlphaNumeric("1enrys");
		assertFalse("Invalid Login", result);
	}
	@Test
	public void testIsValidLoginNameAlphaNumericBoundaryIn() {
		boolean result = LoginValidator.isValidLoginNameAlphaNumeric("h2nrys");
		assertTrue("Invalid Login", result);
	}
	@Test
	public void testIsValidLoginNameAlphaNumericBoundaryOut() {
		boolean result = LoginValidator.isValidLoginNameAlphaNumeric("h2@@rys");
		assertFalse("Invalid Login", result);
	}
	
}
